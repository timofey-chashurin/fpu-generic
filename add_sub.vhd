--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;
use IEEE.MATH_REAL.ALL;

--------------------------------------------------------------------------------
entity add_sub is
	-- see the package declaration for signal descriptions
	generic (OpWidth: 	integer;
 		 ExpWidth: 	integer; 
		 FractWidth: 	integer);
	port(
		opsel:		in 	std_logic_vector(2 downto 0);
		fra_in: 	in 	std_logic_vector(FractWidth+4 downto 0);
		frb_in: 	in 	std_logic_vector(FractWidth+4 downto 0);
		siga:		in	std_logic;
		sigb:		in	std_logic;
		fres_o:		out	std_logic_vector(FractWidth+4 downto 0);
		adsign:		out	std_logic 
		);
end;


architecture Behavioral of add_sub is

signal 	efra:		unsigned(FractWidth+4 downto 0) := (others => '0');
signal 	efrb: 		unsigned(FractWidth+4 downto 0) := (others => '0');
signal 	asign, bsign:	std_logic := '0';
signal	fra_lt_frb:	std_logic := '0';
signal	s_addop:	std_logic := '0';
signal	addsub:		std_logic := '0';
signal 	fres:		unsigned(FractWidth+4 downto 0):=(others=>'0');
signal	res_sign:	std_logic := '0';
--------------------------------------------------------------------------------
begin
	efra <= unsigned(fra_in);	-- read A fraction
	efrb <= unsigned(frb_in);	-- read B fraction
	asign <= siga;			-- read A sign
	bsign <= sigb;			-- read B sign
	
	addsub <= '1' when opsel = "001" else '0'; -- which op is required?
	
	fra_lt_frb <= '1' when (efra > efrb) else '0'; --define the larger fraction
	
	s_addop <= ((not addsub) and (asign xor bsign)) or
		(addsub and (not (asign xor bsign))); -- the actual operation
					
	res_sign <= (fra_lt_frb and asign) xor ((not fra_lt_frb) and -- sign
		(((not addsub)	and bsign) xor (addsub and (not bsign))));
--------------------------------------------------------------------------------
-- add/substract
process(efra, efrb, s_addop, fra_lt_frb)
begin
	if (s_addop = '0') then
		fres <= efra + efrb;
	else
		if (fra_lt_frb = '1') then
			fres <= efra - efrb;
		else
			fres <= efrb - efra;
		end if;
	end if;
end process;
--------------------------------------------------------------------------------
	-- output the result and sign
	fres_o <= std_logic_vector(fres);
	adsign <= res_sign;

end Behavioral;
--------------------------------------------------------------------------------
