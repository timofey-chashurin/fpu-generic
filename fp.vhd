--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.fpu_pack.all;

entity fpu_main is
    generic (OpWidth:    integer; 
             ExpWidth:   integer;
			 FractWidth: integer);
	port(
		clk: 		in 	std_logic;
		rst:		in	std_logic;
		op_a:		in 	std_logic_vector(OpWidth-1 downto 0);
		op_b:		in 	std_logic_vector(OpWidth-1 downto 0);
		opsel:		in 	std_logic_vector(2 downto 0);
		rndm:		in	std_logic_vector(2 downto 0);
		start:		in	std_logic;
		ready:		out	std_logic;
		div_0:		out 	std_logic;
		overflow:	out	std_logic; 
		underflow:	out	std_logic;
		result:	 	out	std_logic_vector(OpWidth-1 downto 0)
	);
end fpu_main;

architecture Behavioral of fpu_main is

----------------------------------------
component denorm_add_sub is
	generic (OpWidth: 	integer; 
		 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
	port(
		clk:		in 	std_logic;
		op_a: 		in 	std_logic_vector(OpWidth-1 downto 0);
		op_b: 		in 	std_logic_vector(OpWidth-1 downto 0);
     		fra_o: 		out 	std_logic_vector(FractWidth+4 downto 0);
		frb_o: 		out 	std_logic_vector(FractWidth+4 downto 0);
		siga:		out	std_logic;
		sigb:		out	std_logic;
		exp_o: 		out 	std_logic_vector(ExpWidth downto 0);
		sh_cnt: 	out  	std_logic_vector(fr_log_width-1 downto 0); -- shift count
      		sh_init: 	out  	std_logic_vector (FractWidth+4 downto 0); -- shift input
      		shifted: 	in 	std_logic_vector (FractWidth+4 downto 0); -- shift output
		dexp_eq:	out	std_logic;
		frsel:		in	std_logic
 	    );
end component;
 
----------------------------------------
component add_sub is
	generic (OpWidth: integer;
		 ExpWidth: integer;
		 FractWidth: integer
	 	);
	port(
		opsel:		in 	std_logic_vector(2 downto 0);
		fra_in: 	in 	std_logic_vector(FractWidth+4 downto 0);
		frb_in: 	in 	std_logic_vector(FractWidth+4 downto 0);
		siga:		in	std_logic;
		sigb:		in	std_logic;
		fres_o:		out	std_logic_vector(FractWidth+4 downto 0);
		adsign:		out	std_logic
	    );
end component;

----------------------------------------
component multiplier is
	generic (OpWidth:	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
	port(
		clk: 		in 	std_logic;
		rst: 		in 	std_logic;
		opsel:		in 	std_logic_vector(2 downto 0);
		op_a: 		in 	std_logic_vector(OpWidth-1 downto 0);
		op_b: 		in 	std_logic_vector(OpWidth-1 downto 0);
	     	mfres_o: 	out 	std_logic_vector(FractWidth+1 downto 0);
		mexp_o: 	out 	std_logic_vector(ExpWidth downto 0);
		msign:		out	std_logic
	    );
end component;

----------------------------------------
component divider is
	generic (OpWidth: 	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
	port(
		clk: 		in 	std_logic;
		rst: 		in 	std_logic;
		opsel:		in 	std_logic_vector(2 downto 0);
		op_a: 		in 	std_logic_vector(OpWidth-1 downto 0);
		op_b: 		in 	std_logic_vector(OpWidth-1 downto 0);
		start:		in	std_logic;
		ready:		out	std_logic;
	        dres_o: 	out 	std_logic_vector(FractWidth+2 downto 0);
		dexp_o: 	out 	std_logic_vector(ExpWidth downto 0);
		div_sign:	out	std_logic;
		div_zero:	out	std_logic
 	    );
end component;

----------------------------------------
component exponent_mux is
	generic (OpWidth: 	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
	port(		
		opsel:		in	std_logic_vector(2 downto 0);
		ads_exp: 	in	std_logic_vector(ExpWidth downto 0);
		mul_exp: 	in	std_logic_vector(ExpWidth downto 0);
		div_exp: 	in	std_logic_vector(ExpWidth downto 0);
		fin_exp: 	out	std_logic_vector(ExpWidth downto 0);
		fract1_in:	in	std_logic_vector(FractWidth+4 downto 0);
		fract2_in:	in	std_logic_vector(FractWidth+4 downto 0);
		sh_dn:		in	std_logic_vector(fr_log_width-1 downto 0);
		sh_pn:		in	std_logic_vector(fr_log_width-1 downto 0);
		fract_sel:	in	std_logic;
		left_sel:	in	std_logic;
		sh_out:		out	std_logic_vector(fr_log_width-1 downto 0);
		fr_out:		out	std_logic_vector(FractWidth+4 downto 0);
		left_so:	out	std_logic
	    );
end component;

----------------------------------------
component post_normalizer is
	generic (OpWidth: 	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
	port(	
		clk: 		in 	std_logic;
		rst:		in	std_logic;
		opsel:		in	std_logic_vector(2 downto 0);
		rnd_mode:	in	std_logic_vector(2 downto 0);
		as_res: 	in	std_logic_vector(FractWidth+4 downto 0);
		asign:		in	std_logic;
		mult_res: 	in	std_logic_vector(FractWidth+1 downto 0);
		msign:		in	std_logic;
		div_res: 	in	std_logic_vector(FractWidth+2 downto 0);
		dsign: 		in	std_logic;
		div_zero: 	in	std_logic;
		pre_exp: 	in	std_logic_vector(ExpWidth downto 0);
		opa:		in	std_logic_vector(OpWidth-1 downto 0);
		opb:		in	std_logic_vector(OpWidth-1 downto 0);
		pres_o:		out	std_logic_vector(FractWidth-1 downto 0);
		pexp_o:		out	std_logic_vector(ExpWidth-1 downto 0);
		ovf:		out	std_logic;
		unf:		out	std_logic;
		div0:		out	std_logic;
		psign_o:	out	std_logic;
		sh_cnt_pn: 	out  	std_logic_vector(fr_log_width-1 downto 0);
      		sh_init_pn: 	out  	std_logic_vector(FractWidth+4 downto 0);
		sh_left_pn:	out	std_logic;
      		shifted: 	in 	std_logic_vector(FractWidth+4 downto 0);
		ready:		in	std_logic;
		pn_need:	out	std_logic
 	    );
end component;

----------------------------------------
component bar_shifter is
    	generic (OpWidth: 	integer;
	     	 ExpWidth:	integer;
		 FractWidth: 	integer
	 	);
    	port (
        	shiftIn: 	in 	std_logic_vector(FractWidth+4 downto 0);
        	numShifts: 	in 	std_logic_vector(fr_log_width-1 downto 0);
		left:		in  	std_logic;
        	shiftOut: 	out 	std_logic_vector(FractWidth+4 downto 0)
    	     );
end component;

----------------------------------------
component bsh_fsm is
    	generic (OpWidth: 	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
    	port(
        	clk: 		in 	std_logic;
		rst: 		in 	std_logic;
		opsel:		in	std_logic_vector(2 downto 0);
		start:		in  	std_logic;
        	fsm_sel:	out 	std_logic;
		fsm_rdy:	out	std_logic;
		fexp_eq:	in	std_logic;
		need_pn:	in	std_logic;
		dres_rdy:	in	std_logic
    	    );
end component;

----------------------------------------------------------------
signal 	exp_norm: 		std_logic_vector(ExpWidth downto 0) := (others => '0');
signal 	fra_ext, frb_ext: 	std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal 	a, b, c: 		std_logic := '0';
signal	sdexp_eq:		std_logic := '0';

----------------------------------------------------------------
signal	ads_res:	std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	ads_exp:	std_logic_vector(ExpWidth downto 0) := (others => '0');
signal	as_sign:	std_logic := '0';
signal	signa:		std_logic := '0';
signal	signb:		std_logic := '0';

----------------------------------------------------------------
signal	mul_res:	std_logic_vector(FractWidth+1 downto 0) := (others => '0');
signal	mul_exp:	std_logic_vector(ExpWidth downto 0) := (others => '0');
signal	mult_ov:	std_logic := '0';
signal	mult_un:	std_logic := '0';
signal	mul_sign:	std_logic := '0';

----------------------------------------------------------------
signal	div_res:	std_logic_vector(FractWidth+2 downto 0) := (others => '0');
signal	div_exp:	std_logic_vector(ExpWidth downto 0) := (others => '0');
signal	st:		std_logic := '0';
signal	div_rdy:	std_logic := '0';
signal	dsign:		std_logic := '0';
signal	div_zero:	std_logic := '0';

----------------------------------------------------------------
signal	postn_res:	std_logic_vector(FractWidth-1 downto 0) := (others => '0');
signal	psign:		std_logic := '0';
signal	need_spn:	std_logic := '0';

----------------------------------------------------------------
signal	exp_mid:	std_logic_vector(ExpWidth downto 0) := (others => '0');
signal	res_out:	std_logic_vector(FractWidth-1 downto 0) := (others => '0');
signal	exp_out:	std_logic_vector(ExpWidth-1 downto 0) := (others => '0');
signal	sovf, sunf, sdiv0:	std_logic := '0';

----------------------------------------------------------------
-- Fraction multiplexer signals
signal	frd_in:		std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	frp_in:		std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	sh_dnorm:	std_logic_vector(fr_log_width-1 downto 0) := (others => '0');
signal	sh_pnorm:	std_logic_vector(fr_log_width-1 downto 0) := (others => '0');
signal	shift_o:	std_logic_vector(fr_log_width-1 downto 0) := (others => '0');
signal	left_in:	std_logic := '0';

-- Barrel Shifter signals
signal	bsh_inp:	std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	bsh_out:	std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	shft_num:	std_logic_vector(fr_log_width-1 downto 0) := (others => '0');
signal	left_sel:	std_logic := '0';

-- BS FSM
signal	fract_sel:	std_logic := '0';
signal	fr_hold:	std_logic := '0';
signal	op_sel:		std_logic_vector(2 downto 0) := (others => '0');
signal	round_mode:	std_logic_vector(2 downto 0) := (others => '0');

----------------------------------------------------------------------
begin
-- Normalizer of FP numbers
Denormalizer_comp: denorm_add_sub
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)
port map (clk, op_a, op_b, fra_ext, frb_ext, signa, signb,
	  exp_norm, sh_dnorm, frd_in, bsh_out, sdexp_eq, fract_sel);

-- Adder/Subtracter
Adder_Sub: add_sub
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)
port map (opsel, fra_ext, frb_ext, signa, signb, ads_res, as_sign);

-- Multiplier
Mult: multiplier
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)
port map (clk, rst, opsel, op_a, op_b, mul_res, mul_exp, mul_sign);

-- Divider
Div: divider
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)
port map (clk, rst, opsel, op_a, op_b, start, div_rdy,
	  div_res, div_exp, dsign, div_zero);

-- Exponent multiplexer
Mux: exponent_mux
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)	
port map (opsel, exp_norm, mul_exp, div_exp, exp_mid, frd_in, frp_in,
          sh_dnorm, sh_pnorm, fract_sel, left_in, shft_num, bsh_inp, left_sel);

-- Post normaliser
PostNorm: post_normalizer
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)
port map (clk, rst, opsel, round_mode, ads_res, as_sign, mul_res, mul_sign, div_res, dsign,
	  div_zero, exp_mid, op_a, op_b, res_out, exp_out, sovf, sunf, sdiv0, psign,
	  sh_pnorm, frp_in, left_in, bsh_out, fr_hold, need_spn);

-- Barrel Shifter
Barrel_shifter: bar_shifter
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)
port map (bsh_inp, shft_num, left_sel, bsh_out);

-- FSM
Bshifter_fsm: bsh_fsm
generic map (OpWidth => OpWidth, ExpWidth => ExpWidth, FractWidth => FractWidth)
port map (clk, rst, opsel, start, fract_sel, fr_hold, sdexp_eq, need_spn, div_rdy);

	
process (rst, clk, res_out, exp_out, psign, sovf, sunf, sdiv0, fr_hold, mul_exp, mul_res,
           exp_norm, fra_ext, div_res, div_exp)
begin
	if (rst = '1') then
		result <= (others => '0');
		overflow <= '0';
		underflow <= '0';
		div_0 <= '0';
		ready <= '0';
	else
		-- Output the FP result
		result <= psign & exp_out & res_out;
        
		--Overflow condition signal
		overflow <= sovf;
		-- Underflow condition signal
		underflow <= sunf;
		-- Division by 0
		div_0 <= sdiv0;
		-- output data ready signal
		ready <= fr_hold;
	end if;
end process;

end Behavioral;
