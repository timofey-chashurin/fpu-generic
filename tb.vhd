--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************
 
LIBRARY ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.ALL;
use ieee.math_real.all;
use work.fpu_pack.all; 

ENTITY tb IS  
END tb;
  
ARCHITECTURE behavior OF tb IS 

function conv_to_fp (fp_res_o: std_logic_vector (OpWidth-1 downto 0) ) return real is
variable res:	real; 
begin
	if (fp_res_o(OpWidth-1) = '1') then
		res := real((2.0 ** (real(to_integer(signed(fp_res_o(OpWidth-2 downto (OpWidth-1-ExpWidth))))))) * 
		((real(to_integer(unsigned(fp_res_o(FractWidth-1 downto 0))))) / (2.0 ** (real(FractWidth))))) * (-1.0);
	else 
		res := real((2.0 ** (real(to_integer(signed(fp_res_o(OpWidth-2 downto (OpWidth-1-ExpWidth))))))) * 
		((real(to_integer(unsigned(fp_res_o(FractWidth-1 downto 0))))) / (2.0 ** (real(FractWidth)))));
	end if; 
	return res;
end conv_to_fp;
 
    -- Component Declaration for the Unit Under Test (UUT)
	COMPONENT fpu_main
   PORT(
         clk : 		in  	std_logic;
         rst : 		in  	std_logic; 
         op_a : 	in  	std_logic_vector(OpWidth-1 downto 0);
         op_b : 	in  	std_logic_vector(OpWidth-1 downto 0);
         opsel: 	in  	std_logic_vector(2 downto 0);
	 rndm:		in	std_logic_vector(2 downto 0);
	 start:		in	std_logic;
	 ready:		out	std_logic;
         div_0 : 	out 	std_logic;
         overflow : 	out  	std_logic;
         underflow : 	out  	std_logic;
	 result:	out	std_logic_vector(OpWidth-1 downto 0)
        );
	END COMPONENT;
    
   --Inputs
	signal clk : 	std_logic := '0';
   	signal rst : 	std_logic := '0';
   	signal op_a : 	std_logic_vector(OpWidth-1 downto 0) := (others => '0');
   	signal op_b : 	std_logic_vector(OpWidth-1 downto 0) := (others => '0');
   	signal opsel :	std_logic_vector(2 downto 0) := (others => '0');
	signal rndm:	std_logic_vector(2 downto 0) := (others => '0');
	signal start: 	std_logic := '0';
	signal ready: 	std_logic := '0';

 	--Outputs
   	signal div_0: 		std_logic;
   	signal overflow: 	std_logic;
   	signal underflow: 	std_logic;
	signal result: 		std_logic_vector(OpWidth-1 downto 0);
	
	signal assert_fp_res: 	std_logic_vector(OpWidth-1 downto 0) := (others => '0');
	signal init_val: 	std_logic := '0';
	
	signal op_a_fp:		real;
	signal op_b_fp:		real;
	signal result_fp: 	real;
	signal rand_sig : 	real := 0.0;
	
	signal r_inc : 		unsigned(2 downto 0) := (others => '0');
 
   -- Clock period definitions 
   constant clk_period : time := 10 ns;
BEGIN
	-- Instantiate the Unit Under Test (UUT)
   uut: fpu_main PORT MAP (
        clk => clk,
        rst => rst,
        op_a => op_a,
        op_b => op_b,
        opsel => opsel,
	rndm => rndm,
	start => start,
	ready => ready,
        div_0 => div_0,
        overflow => overflow,
        underflow => underflow,
	result => result
        );

   -- Clock process definitions
   clk_process :process
   begin
	clk <= '0';
	wait for clk_period/2;
	clk <= '1';
	wait for clk_period/2; 
   end process;
	
	op_a_fp <= conv_to_fp(op_a);
	op_b_fp <= conv_to_fp(op_b);
	result_fp <= conv_to_fp(result);
		
process
    variable seed1, seed2: positive;              
    variable rand: real;  
begin
    uniform(seed1, seed2, rand);   -- generate a random number
    rand_sig <= rand;
	 r_inc <= unsigned(rndm) + 1;
    wait for 1 ns;
end process;


   -- Stimulus process 
   stim_proc: process
   begin
	rst <= '1';
	wait for clk_period*1;
	rst <= '0';
	wait for clk_period*1;
	wait for clk_period/2;
	
		-----------------------------------------------------
		-- Multiplication testing
		opsel <= "010";
		rndm <= "010";
				
--		rndm <= "000";
--		for i in 0 to 4 loop
		
		-----------------------------------------------------	
		-- mult generic 1
		-- (-0.25) * (-0.0625) = 0.015625
		-- operand A
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-2) <= (others => '1');
		op_a(FractWidth-2 downto 0) <= (others => '0');

		-- operand B
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '1';
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <= (others => '1');
		op_b(FractWidth-3) <= '1';
		
		-- generic assertion signal to check with
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <= (others => '1');
		assert_fp_res(OpWidth-ExpWidth+1) <= '0';
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';

		start <= '1';
		wait for clk_period*1;
		start <= '0';
		
		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Multiplication example 1 is failed: (-0.25) * (-0.0625) = 0.015625"
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Mult. example 1: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Mult. example 1: ovf_o has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Mult. example 1: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-----------------------------------------------------
		
		-----------------------------------------------------
		-- mult generic 2
		-- insert stimulus here -0.5*2^(-2) and 0.25*2^(-15)
		-- (-0.125) * 0.000008 = (-0.000001)
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth) <= (others => '1');
		op_a(FractWidth-1) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '0';
		op_b(OpWidth-2 downto OpWidth-ExpWidth+3) <= (others => '1');
		op_b(OpWidth-ExpWidth-1) <= '1';
		op_b(FractWidth-2) <= '1';
		
		-- generic assertion signal to check with
		assert_fp_res(OpWidth-1 downto OpWidth-ExpWidth-1) <= (others => '1');
		assert_fp_res(OpWidth-ExpWidth) <= '0';
		assert_fp_res(OpWidth-ExpWidth+3) <= '0';
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Mult. example 2: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Mult. example 2: ovf_o has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Mult. example 2: underflow has a wrong value 1"
		severity failure;
		
		start <= '1';
		wait for clk_period*1; 
		start <= '0';
		
		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Multiplication example 2 is failed: (-0.125) * 0.000008 = (-0.000001)"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- mult generic 3
		-- insert stimulus here 0.5*2^2 and 0.25*2^5
		-- 2 * 8 = 16
		op_a <= (others => '0');
		op_a(OpWidth-ExpWidth) <= '1';
		op_a(OpWidth-ExpWidth-2) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-ExpWidth-1) <= '1';
		op_b(OpWidth-ExpWidth+1) <= '1';
		op_b(OpWidth-ExpWidth-3) <= '1';
		
		-- generic assertion signal to check with
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-ExpWidth-1) <= '1';
		assert_fp_res(OpWidth-ExpWidth+1) <= '1';
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';
		
		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Multiplication example 3 is failed: 2 * 8 = 16"
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Mult. example 3: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Mult. example 3: ovf_o has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Mult. example 3: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- mult generic 4
		-- (-0.5*2^(-2)) * 0.5625*2^0 = 0.5625*2^(-3)
		-- (-0.125) * 0.5625 = (-0.0703125)
		op_a <= (others => '0');
		op_a(OpWidth-1 downto OpWidth-ExpWidth-1) <= (others => '1');
		op_a(OpWidth-ExpWidth-1) <= '0';
		op_a(OpWidth-ExpWidth-2) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-5) <= "1001";

		-- generic assertion signal to check with
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-1 downto OpWidth-ExpWidth+1) <= (others => '1');
		assert_fp_res(OpWidth-ExpWidth downto OpWidth-ExpWidth-1) <= "01";
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-5) <= "1001";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';
		
		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Multiplication example 4 is failed: (-0.125) * 0.5625 = (-0.0703125)"
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Mult. example 4: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Mult. example 4: ovf_o has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Mult. example 4: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- mult generic 5
		-- (-0.25*2^50) * (-0.375*2^45 = 0.75*2^92
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(20, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-3) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '1';
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(12, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-3 downto OpWidth-ExpWidth-4) <= "11";

		-- generic assertion signal to check with
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(29, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Multiplication example 5 is failed..."
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Mult. example 5: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Mult. example 5: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Mult. example 5: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- mult generic 6
		-- 0.5*2^MAX_EXP * 0.5*2^1 = OVERFLOW
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(((2**(ExpWidth-1))-1),
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2) <= '1';

		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(2, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2) <= '1';

		-- generic assertion signal to check the overflow
		assert_fp_res <= (others => '1');
		assert_fp_res(OpWidth-1) <= '0';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Mult. ex. 6: overflow res and exp must contain all ones"
--		severity ERROR;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Mult. example 6: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '1')
		report "Mult. example 6: ovf_o has a wrong value 0"
		severity failure;
		
		assert (underflow = '0')
		report "Mult. example 6: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-------------------------------------------------
		-- mult generic 7
		-- 0.25*2^MIN_EXP * 0.5*2^1 = UNDERFLOW
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(2**(ExpWidth-1)),
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-3) <= '1';

		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2) <= '1';

		-- generic assertion signal to check the underflow
		assert_fp_res <= (others => '0');
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Mult. ex. 7: underflow res and exp must contain all zeros"
		severity ERROR;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Mult. example 6: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Mult. example 6: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '1')
		report "Mult. example 6: underflow has a wrong value 0"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		--	rndm <= std_logic_vector(unsigned(rndm) + 1);
		--	end loop; 
		-------------------------------------------------
		
		----------------------------------------------------------
		-- Division testing
		opsel <= "011";
		rndm <= "010";
		-------------------------------------------------
		--rndm <= "000";
		--for i in 0 to 4 loop
		-------------------------------------------------
		-- Div generic 1
		-- 0.25*2^0 / 0.375*2^0
		-- 0.25 / 0.375 = 0.666667
		op_a <= (others => '0');
		op_a(OpWidth-ExpWidth-3) <= '1';

		op_b <= (others => '0');
		op_b(OpWidth-ExpWidth-3 downto OpWidth-ExpWidth-4) <= "11";

		-- generic assertion signal to check the result
		--	assert_fp_res <= (others => '0');
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Div. ex. 1 is failed: 0.25 / 0.375 = 0.666667"
--		severity failure;

		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Div. example 1: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Div. example 1: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Div. example 1: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-- rnd_mode_i <= std_logic_vector(unsigned(rnd_mode_i) + 1);
		-- end loop;
		-------------------------------------------------

		-------------------------------------------------
		-- Div generic 2
		-- 0.5*2^10 / 0.75*2^8
		-- 512 / 192 = 2.666667
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(10, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(8, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";

		-- NOT Generic
--		assert_fp_res <= (others => '0');
--		assert_fp_res(FractWidth-1 downto 0) <= "10101010101010101010101";
--		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
--		std_logic_vector(to_signed(2, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Div. ex. 2 is failed: 512 / 192 = 2.666667"
--		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Div. example 2: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Div. example 2: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Div. example 2: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------

		-------------------------------------------------
		-- Div generic 3
		-- (-0.625)*2^4 / 0.875*2^1 = 
		-- (-10) / 1.75 = -5.714233
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(4, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "101";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(1, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";

--		assert_fp_res <= (others => '0');
--		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
--		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Div. ex. 3 is failed: ..."
--		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Div. example 3: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Div. example 3: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Div. example 3: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Div generic 4 - Division by zero
		-- 0.5*2^5 / 0
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2) <= '1';
		
		op_b <= (others => '0');

		assert_fp_res <= (others => '0');

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Div. ex. 4 is failed: ..."
		severity error;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '1') 
		report "Div. example 4: div_0 has a wrong value 0"
		severity failure;
		
		assert (overflow = '0')
		report "Div. example 4: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Div. example 4: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Div generic 5 - Overflow
		-- 0.5*2^MAX / 0.875*2^(-1)
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(((2**(ExpWidth-1))-1), op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-1, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		assert_fp_res <= (others => '0');

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Div. ex. 5 is failed: ..."
--		severity error;
	
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Div. example 5: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '1')
		report "Div. example 5: overflow has a wrong value 0"
		severity failure;
		
		assert (underflow = '0')
		report "Div. example 5: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Div generic 6 - Underflow
		-- 0.75*2^MIN / 0.625*2^1
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(2**(ExpWidth-1)), op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "101";
		
		assert_fp_res <= (others => '0');

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Div. ex. 6 is failed: ..."
--		severity error;
		 
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Div. example 6: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Div. example 6: overflow has a wrong value 1"
		severity failure; 
		
		assert (underflow = '1')
		report "Div. example 6: underflow has a wrong value 0"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		
		-------------------------------------------------
		------------	Denormalization unit	 	----------
		-------------------------------------------------
		opsel <= "000";
		-------------------------------------------------
		
		-------------------------------------------------
		-- Den 1
		-- 0.5*2^7 and 0.25*2^6
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(6, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-3) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		---------------------------------------------------
		
		---------------------------------------------------
		-- Den 2
		-- 0.25*2^11 and 0.5*2^7
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(11, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-3) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		---------------------------------------------------
		
		---------------------------------------------------
		-- Den 3
		-- 0.75*2^10 and 0.5*2^12
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(10, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(12, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		---------------------------------------------------
		
		---------------------------------------------------
		-- Den 4
		-- 0.625*2^9 and 0.25*2^9
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(9, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "101";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(9, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-3) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		
		
		---------------------------------------------------
		-- Den 5
		-- 0.875*2^(-7) and 0.5*2^(-5)
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
	
		
		-------------------------------------------------
		------------	Addition and Subtraction	-------
		-------------------------------------------------
		-- Addition
		opsel <= "000";
		-------------------------------------------------
		-------------------------------------------------
		-- Add generic 1
		-- 0.75*2^3 + 0.15625*2^3
		-- (-6) + 1.25 = (-4.75)
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-4 downto OpWidth-ExpWidth-6) <= "101";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-1) <= '1';
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-6) <= "10011";

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Add. ex. 1 is failed: (-6) + 1.5 = (-4.75)"
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Add. example 1: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Add. example 1: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Add. example 1: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Add generic 2
		-- 96 + (-7.25) = 88.75
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '1';
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-6) <= "11101";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-10) <= "101100011";

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Add. ex. 2 is failed: 96 + (-7.25) = 88.75"
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Add. example 2: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Add. example 2: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Add. example 2: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------

		-------------------------------------------------
		-- Add generic 3
		-- (-12) + (-0.75) = (-12.75)
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(4, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '1';
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(0, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-1) <= '1';
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(4, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-7) <= "110011";

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Add. ex. 3 is failed: (-12) + (-0.75) = (-12.75)"
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Add. example 3: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Add. example 3: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Add. example 3: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Add generic 4
		-- 5.625 + (-29) = -23.375
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-7) <= "101101";
		
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '1';
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-6) <= "11101";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-1) <= '1';
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-9) <= "10111011";

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Add. ex. 4 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Add. example 4: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Add. example 4: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Add. example 4: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Add generic 5
		-- 112 + 6 = 118
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-7) <= "111011";

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Add. ex. 5 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Add. example 5: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Add. example 5: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Add. example 5: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Add generic 6
		-- (-7) + 7 = 0
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		assert_fp_res <= (others => '0');

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Add. ex. 6 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Add. example 6: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Add. example 6: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Add. example 6: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Add generic 7
		-- Overflow
		-- 0.875*2^MAX + 0.625*2^MAX = Overflow
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(((2**(ExpWidth-1))-1), op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(((2**(ExpWidth-1))-1), op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "101";
		
		assert_fp_res <= (others => '1');
		assert_fp_res(OpWidth-1) <= '0';

		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Add. ex. 7 is failed: "
--		severity error;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Add. example 7: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '1')
		report "Add. example 7: overflow has a wrong value 0"
		severity failure;
		
		assert (underflow = '0')
		report "Add. example 7: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-------------------------------------------------
		-- Subtraction
		opsel <= "001";
		-------------------------------------------------
		-- Sub generic 1
		-- 
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-4, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-3 downto OpWidth-ExpWidth-4) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(4, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-3 downto OpWidth-ExpWidth-5) <= "101";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-1) <= '1';
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-11) <= "1001111101";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Sub. ex. 1 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Sub. example 1: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Sub. example 1: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Sub. example 1: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Sub generic 2
		-- 48 - 112 = (-64)
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(6, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-1) <= '1';
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Sub. ex. 2 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Sub. example 2: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Sub. example 2: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Sub. example 2: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Sub generic 3
		-- (-1.5) - (-7) = 5.5
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(1, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '1';
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-5) <= "1011";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Sub. ex. 3 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Sub. example 3: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Sub. example 3: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Sub. example 3: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Sub generic 4
		-- 96 - (-112) = 208
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		op_b <= (others => '0');
		op_b(OpWidth-1) <= '1';
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "111";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(8, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-5) <= "1101";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Sub. ex. 4 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Sub. example 4: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Sub. example 4: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Sub. example 4: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Sub generic 5
		-- (-0.953125)*2^3 - 0.890625*2^3 = 0.921875*2^4
		-- (-7.625) - 7.125 = (-14.75)
		op_a <= (others => '0');
		op_a(OpWidth-1) <= '1';
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-7) <= "111101";
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-7) <= "111001";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-1) <= '1';
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(4, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-7) <= "111011";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Sub. ex. 5 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Sub. example 5: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Sub. example 5: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Sub. example 5: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Sub generic 6. Underflow
		-- 0.5*2^MIN - 0.25*2^MIN
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(2**(ExpWidth-1)), op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2) <= '1';
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(2**(ExpWidth-1)), op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "01";
		
		assert_fp_res <= (others => '0');
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Sub. ex. 6 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Sub. example 6: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Sub. example 6: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '1')
		report "Sub. example 6: underflow has a wrong value 0"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
	
		-------------------------------------------------
		-- Normalization of the operand A
		opsel <= "110";
		-------------------------------------------------
		-- Norm. A generic 1
		-- 0.09375*2^6 = 0.75*2^3
		-- 6
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(6, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-6) <= "00011";
		
		op_b <= (others => '0');
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-6) <= "11000";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. A ex. 1 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. A example 1: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Norm. A example 1: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. A example 1: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Norm. A generic 2
		-- 0.125*2^7 = 0.5*2^5
		-- 16
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "001";
		
		op_b <= (others => '0');
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(5, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. A ex. 2 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. A example 2: div_0 has a wrong value 1"
		severity failure;
		
		assert (overflow = '0')
		report "Norm. A example 2: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. A example 2: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Norm. A generic 3
		-- 0.000152
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(7 downto 0) <= "10011111";
		
		op_b <= (others => '0');
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(FractWidth-11), assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-9) <= "10011111";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. A ex. 3 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. A example 3: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. A example 3: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. A example 3: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Norm. A generic 4
		-- -> 0
		-- 0.000001 = 0.000001
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(0) <= '1';
		
		op_b <= (others => '0');
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(FractWidth-4), assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. A ex. 4 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. A example 4: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. A example 4: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. A example 4: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Norm. A generic 5
		-- 0.000....001*2^MIN = Underflow
		op_a <= (others => '0');
		op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(2**(ExpWidth-1)), op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_a(0) <= '1';
		
		op_b <= (others => '0');
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(FractWidth-4), assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
		--	assert (result = assert_fp_res)
		--	report "Norm. A ex. 5 is failed: "
		--	severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. A example 5: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. A example 5: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '1')
		report "Norm. A example 5: underflow has a wrong value 0"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------

		-------------------------------------------------
		-- Mormalization of B
		opsel <= "111"; 
		-------------------------------------------------
		-- Norm. B generic 1
		-- 0.125*2^7 = 0.5*2^5
		-- 16 = 16
		op_a <= (others => '0');
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(7, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-4) <= "001";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(5, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. B ex. 1 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. B example 1: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. B example 1: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. B example 1: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Norm. B generic 2
		-- 0.09375*2^6 = 0.75*2^3
		-- 6 = 6
		op_a <= (others => '0');
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(6, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-6) <= "00011";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. B ex. 2 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. B example 2: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. B example 2: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. B example 2: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Norm. B generic 3
		-- 0.00006 = 0.00006
		op_a <= (others => '0');
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(5 downto 0) <= "111111";
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(FractWidth - 9), assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-7) <= "111111";
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. B ex. 3 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. B example 3: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. B example 3: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. B example 3: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------
		
		-------------------------------------------------
		-- Norm. B generic 4
		-- -> 0
		-- 0.000001 = 0.000001
		op_a <= (others => '0');
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(3, op_b(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(0) <= '1';
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(FractWidth - 4), assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		
		-- Check the result
		assert (result = assert_fp_res)
		report "Norm. B ex. 4 is failed: "
		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0') 
		report "Norm. B example 4: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. B example 4: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '0')
		report "Norm. B example 4: underflow has a wrong value 1"
		severity failure;
		
		wait for clk_period*1;
		-------------------------------------------------

		-------------------------------------------------
		-- Norm. B generic 5
		-- 0.000....001*2^MIN = Underflow
		op_a <= (others => '0');
		
		op_b <= (others => '0');
		op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(2**(ExpWidth-1)), op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		op_b(0) <= '1';
		
		assert_fp_res <= (others => '0');
		assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1) <=
		std_logic_vector(to_signed(-(FractWidth-4), assert_fp_res(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
		assert_fp_res(OpWidth-ExpWidth-2) <= '1';
		
		start <= '1';
		wait for clk_period*1;
		start <= '0';

		wait until ready = '1';
		wait for clk_period*1;
		-- Check the result
--		assert (result = assert_fp_res)
--		report "Norm. B ex. 5 is failed: "
--		severity failure;
		
		-- Check div_0, ovf and unf flags
		assert (div_0 = '0')
		report "Norm. A example 5: div_0 has a wrong value 1"
		severity failure; 
		
		assert (overflow = '0')
		report "Norm. A example 5: overflow has a wrong value 1"
		severity failure;
		
		assert (underflow = '1')
		report "Norm. A example 5: underflow has a wrong value 0"
		severity failure;

		wait for clk_period*5;


		------------------------------------------------------
		----------------  Rounding testing -------------------
		------------------------------------------------------		
		opsel <= "000";
		
		for j in 0 to 1 loop
		
			rndm <= "000";
			
			op_a <= (others => '0');
			op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
			op_a(0) <= '1';
			
			op_b <= (others => '0');
			op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_b(OpWidth-ExpWidth-2) <= '1';
			
			for i in 0 to 4 loop
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				
				wait until ready = '1';
				wait for clk_period*1;
				
				op_a <= (others => '1');
				op_b <= (others => '1');
				wait for clk_period*1;
				
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				wait until ready = '1';
				wait for clk_period*1;
				
				op_a <= (others => '0');
				op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
				op_a(0) <= '1';
				
				op_b <= (others => '0');
				op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_b(OpWidth-ExpWidth-2) <= '1';

				rndm <= std_logic_vector(r_inc);
			end loop;
			
			rndm <= "000";
			op_a <= (others => '0');
			op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
			op_a(0) <= '1';
			
			op_b <= (others => '0');
			op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_b(OpWidth-ExpWidth-2) <= '1';
			op_b(0) <= '1';
			
			for i in 0 to 4 loop
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				
				wait until ready = '1'; 
				wait for clk_period*1;
				
				op_a <= (others => '1');
				op_b <= (others => '1');
				wait for clk_period*1;
				
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				wait until ready = '1';
				wait for clk_period*1;
				
				op_a <= (others => '0');
				op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
				op_a(0) <= '1';
				
				op_b <= (others => '0');
				op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_b(OpWidth-ExpWidth-2) <= '1';
				op_b(0) <= '1';

				rndm <= std_logic_vector(r_inc);
			end loop;
		
			rndm <= "000";
			op_a <= (others => '0');
			op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
			op_a(0) <= '1';
			
			op_b <= (others => '0');
			op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_b(OpWidth-ExpWidth-2) <= '1';
			op_b(1 downto 0) <= "10";
			
			for i in 0 to 4 loop
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				
				wait until ready = '1';
				wait for clk_period*1;
				
				op_a <= (others => '1');
				op_b <= (others => '1');
				wait for clk_period*1;
				
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				wait until ready = '1';
				wait for clk_period*1;
				
				op_a <= (others => '0');
				op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
				op_a(0) <= '1';
				
				op_b <= (others => '0');
				op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_b(OpWidth-ExpWidth-2) <= '1';
				op_b(1 downto 0) <= "10";

				rndm <= std_logic_vector(r_inc);
			end loop;
			
			rndm <= "000";
			op_a <= (others => '0');
			op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
			op_a(0) <= '1';
			
			op_b <= (others => '0');
			op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
			std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
			op_b(OpWidth-ExpWidth-2) <= '1';
			op_b(1 downto 0) <= "11";
			
			for i in 0 to 4 loop
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				
				wait until ready = '1';
				wait for clk_period*1;
				
				op_a <= (others => '1');
				op_b <= (others => '1');
				wait for clk_period*1;
				
				start <= '1';
				wait for clk_period*1;
				start <= '0';
				wait until ready = '1';
				wait for clk_period*1;
				
				op_a <= (others => '0');
				op_a(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(7, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_a(OpWidth-ExpWidth-2 downto OpWidth-ExpWidth-3) <= "11";
				op_a(0) <= '1';
				
				op_b <= (others => '0');
				op_b(OpWidth-2 downto OpWidth-ExpWidth-1) <=
				std_logic_vector(to_signed(5, op_a(OpWidth-2 downto OpWidth-ExpWidth-1)'length));
				op_b(OpWidth-ExpWidth-2) <= '1';
				op_b(1 downto 0) <= "11";

				rndm <= std_logic_vector(r_inc);
			end loop;
		
		opsel <= "001";
	end loop;
      wait;
   end process;   
END;
