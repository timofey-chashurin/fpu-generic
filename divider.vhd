--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

entity divider is
	generic (OpWidth: 	integer;
	 	 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
	port( 
		clk: 		in 	std_logic;
		rst: 		in 	std_logic;
		opsel:		in 	std_logic_vector(2 downto 0);
		op_a: 		in 	std_logic_vector(OpWidth-1 downto 0);
		op_b: 		in 	std_logic_vector(OpWidth-1 downto 0);
		start:		in	std_logic;
		ready:		out	std_logic;
      		dres_o: 	out 	std_logic_vector(FractWidth+2 downto 0);
		dexp_o: 	out 	std_logic_vector(ExpWidth downto 0);
		div_sign:	out	std_logic;
		div_zero:	out	std_logic
 );
end;

architecture Behavioral of divider is

type 	state_t is	(idle, sub, mul, sres);
signal	state:		state_t := idle;
signal	state_nxt:	state_t := idle;

constant max_iter:	integer := (FractWidth/4);

signal	mult1:		unsigned((((FractWidth+4)*2)-1) downto 0) := (others => '0');
signal	mult1n:		unsigned((((FractWidth+4)*2)-1) downto 0) := (others => '0');
signal	mult2:		unsigned((((FractWidth+4)*2)-1) downto 0) := (others => '0');
signal	mult2n:		unsigned((((FractWidth+4)*2)-1) downto 0) := (others => '0');
signal	cnt, cntn:	integer range 0 to max_iter;

signal	rmr, rmrn:	unsigned(FractWidth+3 downto 0); -- Reminder
signal	num, numn:	unsigned(FractWidth+3 downto 0); -- Numerator(divident)
signal	den, denn:	unsigned(FractWidth+3 downto 0); -- Denominator(divisor)
signal	reslt, resltn:	unsigned(FractWidth+3 downto 0); -- Division result
signal	sready: 	std_logic := '0';

signal 	fra: 		signed(FractWidth-1 downto 0) := (others => '0');
signal 	frb: 		signed(FractWidth-1 downto 0) := (others => '0');
signal 	expa: 		signed(ExpWidth-1 downto 0) := (others => '0');
signal 	expb: 		signed(ExpWidth-1 downto 0) := (others => '0');
signal	sign_a, sign_b: std_logic;
signal	div0: 		std_logic := '0';
signal	fexp:		std_logic_vector(ExpWidth downto 0) := (others => '0');
signal	div_ovf:	std_logic;
signal	div_unf:	std_logic;

constant 	zero:	unsigned(FractWidth-1 downto 0) := (others => '0');
constant	hval:	integer := (((2**ExpWidth-1)-1)/2);
constant	lval:	integer := (-2**(ExpWidth-1));
constant	width:	integer := ExpWidth;
constant 	above:	signed(ExpWidth-1 downto 0) := to_signed(hval, width);
constant 	below:	signed(ExpWidth-1 downto 0) := to_signed(lval, width);

begin
	-- Read the initial fractional and exponent values
	fra <= signed(op_a(FractWidth-1 downto 0));
	frb <= signed(op_b(FractWidth-1 downto 0));
	expa <= signed(op_a(OpWidth-2 downto OpWidth-ExpWidth-1));
	expb <= signed(op_b(OpWidth-2 downto OpWidth-ExpWidth-1));
	sign_a <= op_a(OpWidth-1); -- Read the op A sign
	sign_b <= op_b(OpWidth-1); -- Read the op B sign


process(clk)
begin
	if (clk = '1' and clk'event) then
		if (rst  = '1') then
			state <= idle;
			mult1 <= (others => '0');
			mult2 <= (others => '0');
			reslt <= (others => '0');
			den <=(others => '0');
			num <= (others => '0');
			rmr <= (others => '0');
			cnt <= 0;
		else
			state <= state_nxt;
			mult1 <= mult1n;
			mult2 <= mult2n;
			reslt <= resltn;
			den <= denn;
			num <= numn;
			rmr <= rmrn;
			cnt <= cntn;
		end if;
	end if;
end process;


process(clk, rst, state, cnt, mult1, mult2, reslt, den, num, rmr, fra, frb, start,
	mult1n, mult2n, numn, sign_a, sign_b, div_ovf, div_unf, div0, opsel)
begin
	mult1n <= mult1;
	mult2n <= mult2;
	resltn <= reslt;
	denn <= den;
	numn <= num;
	rmrn <= rmr;
	cntn <= cnt;
	state_nxt <= state;
	
	case state is
		when idle =>
			numn <= unsigned("0000" & fra);
			denn <= unsigned("0000" & frb);
			rmrn <= "0010" & zero;
			cntn <= 0;
						
			if (opsel = "011") then	
				if (start = '1') then
					state_nxt <= sub;
				else 
					state_nxt <= idle;
				end if;
			end if;
			
		when sub =>
			if (cnt >= 1) then
				rmrn <= rmr - mult2(((FractWidth+2)*2)-1 downto FractWidth);
			else
				rmrn <= rmr - den;
			end if;
				state_nxt <= mul;
		
		when mul =>
			mult1n <= num * rmr;
			mult2n <= den * rmr;
			cntn <= cnt + 1;
			rmrn <= "0010" & zero;
			if (cnt >= max_iter) then
				state_nxt <= sres;
			else
				numn <= mult1n(((FractWidth+2)*2)-1 downto FractWidth);
				denn <= mult2n(((FractWidth+2)*2)-1 downto FractWidth);
				state_nxt <= sub;
			end if;
		
		when sres =>
				resltn <= numn;
				state_nxt <= idle;
	end case;
end process;

	sready <= '1' when state = idle else '0';
	div_sign <= sign_a xor sign_b;
	div0 <= '1' when (frb = 0 and (opsel = "011")) else '0';
	div_zero <= div0;

-- Output the result and exponent
process(clk, rst, state, reslt, mult1, mult2, reslt, den, num, rmr, fexp,
	cnt, fra, frb, start, div_unf, div_ovf, div0, resltn, sready)
begin
	if (sready = '1' and state = idle) then
		dres_o <= std_logic_vector(resltn(FractWidth+2 downto 0));
		dexp_o <= fexp;
	else
		dres_o <= (others => '0');
		dexp_o <= (others => '0');
	end if; 
end process;


process (clk, rst, fra, frb, expa, expb, div_ovf, div_unf)

variable expa_u: 	signed(ExpWidth downto 0) := (others => '0');
variable expb_u: 	signed(ExpWidth downto 0) := (others => '0');
variable dexp: 		signed(ExpWidth downto 0) := (others => '0');

begin
if (clk = '1' and clk'event) then
	if (opsel = "011") then	-- if division operation is chosen
		
		-- Expand the exponents
		if(expa(ExpWidth-1) = '1') then
			expa_u := '1' & expa;
		else
			expa_u := '0' & expa;
		end if;
		
		if(expb(ExpWidth-1) = '1') then
			expb_u := '1' & expb;
		else
			expb_u := '0' & expb;
		end if;
		
		-- Calculate the exponent
		dexp := expa_u - expb_u;
	else
		dexp := (others => '0');
	end if;
end if;

	fexp <= std_logic_vector(dexp);

end process;

	ready <= sready;
	
end Behavioral;
