--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use IEEE.NUMERIC_STD.ALL;

use IEEE.math_real.all;
library work;
use work.fpu_pack.all;
 
entity bar_shifter is
    -- see the package declaration for signal descriptions.
	generic (OpWidth: 	integer; 
		 ExpWidth: 	integer;
		 FractWidth: 	integer); 
   port (
        shiftIn:	in	std_logic_vector(FractWidth+4 downto 0);
	numShifts: 	in 	std_logic_vector(fr_log_width-1 downto 0);
	left:		in  	std_logic;
        shiftOut: 	out 	std_logic_vector(FractWidth+4 downto 0)
    ); 
end entity bar_shifter;

	
architecture para_arch of bar_shifter is

type 		std_aoa_type is array(fr_log_width downto 0) of std_logic_vector(FractWidth+4 downto 0);
signal 		p:		std_aoa_type;
signal 		left_in: 	std_logic_vector(FractWidth+4 downto 0);
signal 		left_out: 	std_logic_vector(FractWidth+4 downto 0);

component fixed_shifter is 
	generic(
		FractWidth:	integer;
		MuxWidth:	integer
		); 
	port(
		s_in:	in	std_logic_vector(FractWidth+4 downto 0);
		shft:	in	std_logic;
		left:	in	std_logic;
		s_out:	out	std_logic_vector(FractWidth+4 downto 0)
		);
end component;

begin
	p(0) <= shiftIn;

	stage_gen:
	for s in 0 to (fr_log_width-1) generate
		shift_slice: fixed_shifter
			generic map(FractWidth => FractWidth, MuxWidth => 2**s)
			port map(s_in => p(s), s_out => p(s+1), 
			shft => numShifts(s), left => left);
	end generate;

	shiftOut <= p(fr_log_width);
end para_arch;
