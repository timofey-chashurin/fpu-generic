--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*																							*
--* This file is part of "Generic Synthesizable Floating Point Unit".	*
--*																							*
--* "Generic Synthesizable Floating Point Unit"									*
--* can not be copied and/or distributed without								*
--* the express permission of Tymofii Chashurin									*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

library work;
use work.fpu_pack.all;

entity post_normalizer is
	-- see the package declaration for signal descriptions
	generic (OpWidth: integer;
				ExpWidth: integer;
				FractWidth: integer);
	port(	
		clk: 			in 	std_logic;
		rst:			in		std_logic;
		opsel:		in		std_logic_vector(2 downto 0);
		rnd_mode:	in		std_logic_vector(2 downto 0);
		as_res: 		in		std_logic_vector(FractWidth+4 downto 0);
		asign:		in		std_logic;
		mult_res: 	in 	std_logic_vector(FractWidth+1 downto 0);
		msign:		in		std_logic;
		div_res: 	in		std_logic_vector(FractWidth+2 downto 0);
		dsign: 		in		std_logic;
		div_zero: 	in		std_logic;
		pre_exp: 	in		std_logic_vector(ExpWidth downto 0);
		opa:			in		std_logic_vector(OpWidth-1 downto 0);
		opb:			in		std_logic_vector(OpWidth-1 downto 0);
		pres_o:		out	std_logic_vector(FractWidth-1 downto 0);
		pexp_o:		out	std_logic_vector(ExpWidth-1 downto 0);
		ovf:			out	std_logic;
		unf:			out	std_logic;
		div0:			out	std_logic;
		psign_o:		out	std_logic;
		sh_cnt_pn: 	out  	std_logic_vector(fr_log_width-1 downto 0);
      sh_init_pn:	out  	std_logic_vector(FractWidth+4 downto 0);
		sh_left_pn:	out 	std_logic;
      shifted: 	in 	std_logic_vector(FractWidth+4 downto 0);
		ready:		in		std_logic;
		pn_need:		out	std_logic
	);
end;


architecture Behavioral of post_normalizer is

------------------------------------------
--| FPU Rounding modes:						  |
--| 000 | TowardsZero or truncate 2 LSBs |
--| 001 | to nearest/TiedToEven			  |
--| 010 | to nearest/TiedAwayFromZero	  |
--| 011 | TowardPlusInfinity				  |
--| 100 | TowardMinusInfinity				  |
--| 101 | Reserved					 		  |
--| 110 | Reserved							  |
--| 111 | Reserved							  |
------------------------------------------
function round (fractin: 	std_logic_vector(FractWidth+4 downto 0);
					 round_mode: 	std_logic_vector(2 downto 0);
					 sign: 		std_logic)
					 return std_logic_vector is

variable fract:	std_logic_vector(FractWidth+4 downto 0);
variable rnd_fr:	std_logic_vector(FractWidth+4 downto 0);
variable mode:		std_logic_vector(2 downto 0);
variable rnd:		std_logic;
variable rnd2:		std_logic;

begin
	fract := fractin;
	mode := round_mode;
	
	-- TowardsZero or truncate 2 LSBs
	if (mode = "000") then
		rnd_fr := fract(FractWidth+4 downto 2) & "00";
	
	--to nearest/TiedToEven
	elsif (mode = "001") then
		if (fract(2) = '1') then
			rnd_fr := std_logic_vector(unsigned(fract(FractWidth+4 downto 2)) + 1) & "00";
		else
			rnd_fr := fract(FractWidth+4 downto 2) & "00";
		end if;
		
	-- to nearest/ TiedAwayFromZero
	elsif (mode = "010") then
		rnd := fract(1) or (fract(1) and fract(0));
		if (rnd = '1') then
			rnd_fr := std_logic_vector(unsigned(fract(FractWidth+4 downto 2)) + 1) & "00";
		else
			rnd_fr := fract(FractWidth+4 downto 2) & "00";
		end if;
		
	-- TowardPlusInfinity
	elsif (mode = "011") then
		if (fract(2 downto 0) = "00") then
			rnd_fr := fract(FractWidth+4 downto 2) & "00";
		else
			if (sign = '1') then
				rnd_fr := fract(FractWidth+4 downto 2) & "00";
			else
				rnd_fr := std_logic_vector(unsigned(fract(FractWidth+4 downto 2)) + 1) & "00";
			end if;
		end if;
		
	-- TowardMinusInfinity
		elsif (mode = "100") then
		if (fract(2 downto 0) = "00") then
			rnd_fr := fract(FractWidth+4 downto 2) & "00";
		else
			if (sign = '1') then
				rnd_fr := std_logic_vector(unsigned(fract(FractWidth+4 downto 2)) + 1) & "00";
			else
				rnd_fr := fract(FractWidth+4 downto 2) & "00";
			end if;
		end if;
	end if;
	
	return rnd_fr;
end function round;

constant	hval:			integer := (((2**ExpWidth-1)-1)/2);
constant	lval:			integer := (-2**(ExpWidth-1));
constant	width:		integer := ExpWidth;
constant above:		signed(ExpWidth-1 downto 0) := to_signed(hval, width);
constant below:		signed(ExpWidth-1 downto 0) := to_signed(lval, width);

signal 	sovf, sunf:	std_logic := '0';
signal	ssign:		std_logic := '0';
signal	factin:		std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	op_exp:		signed(ExpWidth-1 downto 0) := (others => '0');
signal	p_fract:		std_logic_vector(FractWidth-1 downto 0) := (others => '0');
signal	p_exp:		std_logic_vector(ExpWidth-1 downto 0) := (others => '0');
signal	p_sign:		std_logic := '0';
signal	spn_need:	std_logic := '0';
signal	rmode:		std_logic_vector(2 downto 0) := (others => '0');
signal	sdiv:			std_logic := '0';

signal	s_sh_left_pn:   std_logic := '0';

signal counter: std_logic_vector(fr_log_width-1 downto 0);


begin
process (opsel, mult_res, pre_exp, msign, dsign, div_res,
			opa, opb, as_res, div_zero, dsign, asign)
begin
		-- Multiplication
		if (opsel = "010") then
			factin <= "000" & mult_res;
			ssign <= msign;
			op_exp <= (others => '0');

		-- Division
		elsif (opsel = "011") then
			factin <= div_res(FractWidth+2 downto 0) & "00";
			ssign <= dsign;
			op_exp <= (others => '0');

		-- Addition and subtraction
		elsif (opsel = "000" or opsel = "001") then
			factin <= as_res;
			ssign <= asign;
			op_exp <= (others => '0');
		
		elsif (opsel = "110") then
			-- Normalize the Operand A
			factin <= "000"  & opa((FractWidth-1) downto 0) & "00";
			op_exp <= signed(opa((OpWidth-2) downto ((OpWidth-2)-(ExpWidth-1))));
			ssign <= opa(OpWidth-1);
			
		elsif (opsel = "111") then
			-- Normalize the Operand B
			factin <= "000"  & opb((FractWidth-1) downto 0) & "00";
			op_exp <= signed(opb((OpWidth-2) downto ((OpWidth-2)-(ExpWidth-1))));
			ssign <= opb(OpWidth-1);
		
		else
			factin <= (others => '0');
			op_exp <= (others => '0');
			ssign <= '0';
		end if;
end process;
   
process (clk, opsel, mult_res, factin, pre_exp, ready, rnd_mode, rmode, s_sh_left_pn,
         sovf, sunf, msign, div_res, shifted, asign, ssign, spn_need, op_exp)

variable		frai:			std_logic_vector(FractWidth+4 downto 0) := (others => '0');
variable		frao:			std_logic_vector(FractWidth+4 downto 0) := (others => '0');
variable		cnt:			unsigned(FractWidth+4 downto 0) := (others => '0');
variable		exp:			signed(ExpWidth downto 0) := (others => '0');
variable		expe:			signed(ExpWidth downto 0) := (others => '0');
variable		as_fra_res:	std_logic_vector(FractWidth-1 downto 0) := (others => '0');
variable		as_exp_res:	std_logic_vector(ExpWidth-1 downto 0) := (others => '0');
variable		rsign:		std_logic := '0';
variable		esign:		std_logic := '0';
 
begin
	spn_need <= factin(FractWidth+4) or factin(FractWidth+3) or
				   factin(FractWidth+2) or (not factin(FractWidth+1));
					 
	frai := factin; 
	
	-- Perform the sign extension procedure for exponent
	if ((opsel = "110") or (opsel = "111")) then
		if (op_exp(ExpWidth-1) = '1') then
			exp := '1' & op_exp;
		else
			exp := '0' & op_exp;
		end if;
	else
		exp := signed(pre_exp);
	end if;

	cnt := (others => '0');
	
	--if rising_edge(clk) then
		rmode <= rnd_mode;
		if (frai(FractWidth+4) = '1') then
			sh_init_pn <= std_logic_vector(frai);
			s_sh_left_pn <= '0';
			cnt := cnt + 3;
			sh_cnt_pn <= std_logic_vector(cnt(fr_log_width-1 downto 0));
			exp := exp + 3;
			frao := shifted;
		
		elsif (frai(FractWidth+3) = '1') then
			cnt := (others => '0');
			sh_init_pn <= std_logic_vector(frai);
			s_sh_left_pn <= '0';
			cnt := cnt + 2;
			sh_cnt_pn <= std_logic_vector(cnt(fr_log_width-1 downto 0));
			exp := exp + 2;
			frao := shifted;

		elsif ((frai(FractWidth+2)) = '1') then -- if overflow
			cnt := (others => '0');
			sh_init_pn <= std_logic_vector(frai);
			s_sh_left_pn <= '0';
			cnt := cnt + 1;
			sh_cnt_pn <= std_logic_vector(cnt(fr_log_width-1 downto 0));
			exp := exp + 1;
			frao := shifted;
			
		elsif((frai(FractWidth+1) = '1')) then -- no norm. is needed
			cnt := (others => '0');
			sh_init_pn <= std_logic_vector(frai);
			frao := factin;

		-- normalization is needed
		else
			cnt := (others => '0');
			sh_init_pn <= std_logic_vector(frai);
			
			s_sh_left_pn <= '1';
			for i in FractWidth+1 downto 0 loop
				if (frai(i) = '1') then
					exit;
				else
					cnt := cnt + 1;	-- how many pos. to shift?
				end if;
			end loop;
			sh_cnt_pn <= std_logic_vector(cnt(fr_log_width-1 downto 0));
			exp := exp - (signed("00" & std_logic_vector(cnt(fr_log_width-1 downto 0))));
			frao := shifted;
		end if;
		
		if (frao = (frao'range => '0')) then
			rsign := '0';
			exp := (others => '0');
		else
			rsign := ssign;
		end if;
	 
		if (exp > above) then
			sovf <= '1';
		else
			sovf <= '0';
		end if;
		if (exp < below) then
			sunf <= '1';
		else
			sunf <= '0';
		end if;
		
		if (sovf = '1') then
			frao := (others => '1');
			exp := (others => '1');
			esign := '0';
		elsif (sunf = '1') then
			frao := (others => '0');
			exp := (others => '0');
			esign := '0';
		else
			esign := ssign;
		end if;
		
		frao := round(frao, rmode, ssign);
		p_fract <= frao(FractWidth+1 downto 2);
		p_exp <= std_logic_vector(exp(ExpWidth-1 downto 0));
		p_sign <= rsign and esign;
        
	--end if;
end process;

-- Output the post-normalized data
process (rst, p_fract, p_exp, p_sign, ready, sovf, sunf)
begin
	if (rst = '1') then
		pres_o <= (others => '0');
		pexp_o <= (others => '0');
		psign_o <= '0';
	else
		if rising_edge(ready) then
			pres_o <= p_fract;	-- Result
			pexp_o <= p_exp;	-- Exponent
			psign_o <= p_sign; 	-- Sign
		end if;
	end if;
end process;

    sh_left_pn <= s_sh_left_pn;

	--Overflow condition signal
	ovf <= sovf;
	 
	-- Underflow condition signal
	unf <= sunf;
	
	-- Division by zero
	sdiv <= div_zero;
	div0 <= sdiv;
	
	pn_need <= spn_need;

end Behavioral;
