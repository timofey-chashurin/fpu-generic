--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.math_real.all;

package fpu_pack is

constant OpWidth : integer := 32;
constant FractWidth: integer := 23;
constant ExpWidth: integer := 8;

constant fr_log_width: integer := integer(ceil(log2(real(FractWidth))));

end fpu_pack;

package body fpu_pack is

end fpu_pack;
