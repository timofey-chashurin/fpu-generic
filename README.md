# fpu-generic

This repository contains the VHDL source files of a generic FPU. The FPU can be synthesized with different fractional and exponential parts widths. The floating-point format is defined in the project VHDL package.
