--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.fpu_pack.all;

entity denorm_add_sub is
	-- see the package declaration for signal descriptions
   	generic (OpWidth: 	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer
		);
  	port(
		clk: 		in 	std_logic;
		op_a: 		in 	std_logic_vector(OpWidth-1 downto 0);
		op_b: 		in 	std_logic_vector(OpWidth-1 downto 0);
		fra_o: 		out 	std_logic_vector(FractWidth+4 downto 0);
		frb_o: 		out 	std_logic_vector(FractWidth+4 downto 0);
		siga:		out	std_logic;
		sigb:		out	std_logic;
		exp_o: 		out 	std_logic_vector(ExpWidth downto 0);
		sh_cnt: 	out  	std_logic_vector(fr_log_width-1 downto 0); -- shift count
        	sh_init:	out  	std_logic_vector(FractWidth+4 downto 0); -- shift input
         	shifted:	in 	std_logic_vector(FractWidth+4 downto 0); -- shift output
		dexp_eq:	out	std_logic;
		frsel:		in	std_logic
		);
end entity denorm_add_sub;
 

architecture Behavioral of denorm_add_sub is

signal	fra, frb:		signed(FractWidth-1 downto 0) := (others => '0');
signal	expa, expb:		signed(ExpWidth-1 downto 0) := (others => '0');
signal	sign_a, sign_b: 	std_logic;
signal	expa_u:			signed(ExpWidth downto 0) := (others => '0');
signal	expb_u:			signed(ExpWidth downto 0) := (others => '0');
signal	exp_eq:			std_logic;
signal	expa_gr_expb:		std_logic;
signal	emux:			std_logic_vector(1 downto 0);
signal	sfra_o: 		std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	sfrb_o: 		std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal	exps_o: 		std_logic_vector(ExpWidth downto 0) := (others => '0');

constant	two_zeros:	std_logic_vector(1 downto 0) := "00";
constant	three_zeros:	std_logic_vector(2 downto 0) := "000";

begin
	expa <= signed(op_a(OpWidth-2 downto OpWidth-ExpWidth-1)); -- take the exponent part from A
	expb <= signed(op_b(OpWidth-2 downto OpWidth-ExpWidth-1)); -- take the exponent part from B
	fra <= signed(op_a(FractWidth-1 downto 0)); -- take the fract. part from A
	frb <= signed(op_b(FractWidth-1 downto 0)); -- take the fract. part from B
	sign_a <= op_a(OpWidth-1); -- Read the op A sign
	sign_b <= op_b(OpWidth-1); -- Read the op B sign
		
	-- Expand the exponent a 
	expa_u <= '1' & expa when (expa(ExpWidth-1) = '1') else '0' & expa;
	
	-- Expand the exponent b
	expb_u <= '1' & expb when (expb(ExpWidth-1) = '1') else '0' & expb;
	
	-- Check if the exponents are equal
	exp_eq <= '1' when (expa = expb) else '0';

	-- Check if expa greater than expb
	expa_gr_expb <= '1' when (expa > expb) else '0';
		
	emux <= exp_eq & expa_gr_expb;
	
	dexp_eq <= exp_eq;

process (clk, expa, expb, emux, expa_u, expb_u, expa_gr_expb,
			fra, frb, exp_eq, shifted, sign_a, sign_b, frsel)

variable	exp_diff:	signed(ExpWidth downto 0);
variable	exp:		signed(ExpWidth downto 0);
variable	shfra:		std_logic_vector(FractWidth+4 downto 0);
variable	shfrb:		std_logic_vector(FractWidth+4 downto 0);
variable	rfra, rfrb:	signed(FractWidth+4 downto 0) := (others => '0');
variable	d_sh_cnt: 	std_logic_vector(fr_log_width-1 downto 0) := (others => '0');

begin
	-- find the exponent difference
	case (emux) is
		when "00" => exp_diff := expb_u - expa_u;
		when "01" => exp_diff := expa_u - expb_u;
		when "10" => exp_diff := (others => '0');
		when others => exp_diff := (others => '1'); -- do not take into account
	end case;
	
	case expa_gr_expb is
		when '0' => exp := expb_u;
		when others => exp := expa_u;
	end case;

	if rising_edge(clk) then
		if (exp_diff > (FractWidth-1)) then
			d_sh_cnt := (others => '0');
			if (expa_gr_expb = '1') then
				shfrb := (others => '0');
				shfra := three_zeros & std_logic_vector(fra) & two_zeros;
			else
				shfra := (others => '0');
				shfrb := three_zeros & std_logic_vector(frb) & two_zeros;
			end if;
		elsif (exp_eq = '1') then
				shfra := three_zeros & std_logic_vector(fra) & two_zeros;
				shfrb := three_zeros & std_logic_vector(frb) & two_zeros;
				d_sh_cnt := (others => '0');
		else
			d_sh_cnt := std_logic_vector(exp_diff(fr_log_width-1 downto 0));
			if (expa_gr_expb = '1') then
				sh_init <= std_logic_vector(three_zeros & std_logic_vector(frb) & two_zeros);
				shfrb := shifted;
				shfra := three_zeros & std_logic_vector(fra) & two_zeros;
			else
				sh_init <= std_logic_vector(three_zeros & std_logic_vector(fra) & two_zeros);
				shfra := shifted;
				shfrb := three_zeros & std_logic_vector(frb) & two_zeros;
			end if;
		end if;
		sfra_o <= std_logic_vector(shfra);
		sfrb_o <= std_logic_vector(shfrb);
		exps_o <= std_logic_vector(exp);
		sh_cnt <= d_sh_cnt;
	end if;
end process;


-- Output the denormalized data
process (clk, frsel, sign_a, sign_b, sfra_o, sfrb_o, exps_o)
begin
	if rising_edge(frsel) then
		fra_o <= sfra_o;
		frb_o <= sfrb_o;
		exp_o <= exps_o;
		siga <= sign_a;
		sigb <= sign_b;
	end if;
end process;

end Behavioral;

