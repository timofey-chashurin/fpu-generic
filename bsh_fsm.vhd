--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bsh_fsm is
	-- see the package declaration for signal descriptions
	generic (OpWidth: 	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer
		);
   port (
          clk: 		in 	std_logic;
 	  rst: 		in 	std_logic;
	  opsel:	in	std_logic_vector(2 downto 0);
	  start:	in  	std_logic;
          fsm_sel:	out 	std_logic;
 	  fsm_rdy:	out	std_logic;
	  fexp_eq:	in	std_logic;
  	  need_pn:	in	std_logic;
	  dres_rdy:	in	std_logic
	);
end bsh_fsm;

architecture Behavioral of bsh_fsm is

type 	state_t is	(idle, calc_st1, calc_st2, postn_st1, postn_st2);
signal	state:		state_t := idle;
signal	state_nxt:	state_t := idle;

signal	sel:		std_logic := '0';
signal	seln:		std_logic := '0';

signal	asrdy:		std_logic := '0';
signal	asrdyn:		std_logic := '0';

signal	fsel1:		std_logic := '0';
signal	fsel1n:		std_logic := '0';
signal	fsel2:		std_logic := '0';
signal	fsel2n:		std_logic := '0';
signal	fsel_xor:	std_logic := '0';

signal	psel1:		std_logic := '0';
signal	psel1n:		std_logic := '0';
signal	psel2:		std_logic := '0';
signal	psel2n:		std_logic := '0';
signal	psel_xor:	std_logic := '0';

begin

process(clk)
begin
	if (clk = '1' and clk'event) then
		if (rst  = '1') then
			state <= idle;
			fsel1 <= '0';
			fsel2 <= '0';
			psel1 <= '0';
			psel2 <= '0';
		else
			state <= state_nxt;
			fsel1 <= fsel1n;
			fsel2 <= fsel2n;
			psel1 <= psel1n;
			psel2 <= psel2n;
		end if;
	end if;
end process;


process(state, seln, sel, start, asrdy, asrdy, psel1, psel2,
		  opsel, fsel1, fsel2, fexp_eq, need_pn, dres_rdy)

begin
	fsel1n <= fsel1;
	fsel2n <= fsel2;
	psel1n <= psel1;
	psel2n <= psel2;
	state_nxt <= state;
	
	case state is
		when idle =>
			if ((opsel = "000") or (opsel = "001")) then
				-- ADD/SUB
				if (start = '1') then
					state_nxt <= calc_st1;
					if (fexp_eq = '1') then
						fsel1n <= '1';
					else
						fsel1n <= '0';
					end if;
				else	
					state_nxt <= idle;
				end if;
				-- End of ADD/SUB
			
			elsif ((opsel = "010") or (opsel = "011")) then
				-- MULT and DIV
				if (start = '1') then
					fsel1n <= '1';
					psel1n <= '0';
					state_nxt <= calc_st2;
				else
					state_nxt <= idle;
				end if;
				-- End of MULT/DIV
				
			elsif ((opsel = "110") or (opsel = "111") or (opsel = "100")) then
				-- Norm AB
				if (start = '1') then
					fsel1n <= '1';
					fsel2n <= '0';
					psel1n <= '0';
					psel2n <= '0';
					state_nxt <= postn_st1;
				else
					state_nxt <= idle;
				end if;
				-- End of Norm AB

			end if;

		when calc_st1 =>
			-- ADD/SUB
			if ((sel = '1') and (need_pn = '0') and (fexp_eq = '1')) then
				fsel2n <= '0';
				psel1n <= '1';
				state_nxt <= postn_st2;
			elsif (fexp_eq = '1') then
				fsel2n <= '0';
				psel1n <= '0';
				state_nxt <= postn_st1;
			else
				fsel2n <= '1';
				psel1n <= '0';
				state_nxt <= calc_st2;
			end if;
			-- End of ADD/SUB

		when calc_st2 =>
			if ((opsel = "000") or (opsel = "001")) then
				-- ADD/SUB
				psel2n <= '0';
				if (need_pn = '1') then
					psel1n <= '0';
					state_nxt <= postn_st1;
				else
					psel1n <= '1';
					state_nxt <= postn_st2;
				end if;
				-- End of ADD/SUB
			
			elsif (opsel = "010") then
				-- MULT
				fsel1n <= '0';
				fsel2n <= '0';
				psel1n <= '0';
				psel2n <= '0';
				state_nxt <= postn_st1;
				-- End of MULT
				
			elsif (opsel = "011") then
				-- DIV
				if (dres_rdy = '1') then
					if (need_pn = '1') then
						fsel1n <= '1';
						fsel2n <= '0';
						psel1n <= '0';
						psel2n <= '0';
						state_nxt <= postn_st1;
					else
						fsel1n <= '1';
						fsel2n <= '0';
						psel1n <= '0';
						psel2n <= '1';
						state_nxt <= postn_st2;
					end if;
				else 
					state_nxt <= calc_st2;
				end if;
				-- End of DIV				
			end if;

		when postn_st1 =>
			if ((opsel = "000") or (opsel = "001")) then
				-- ADD/SUB
				psel2n <= '1';
				state_nxt <= postn_st2;
				-- End of ADD/SUB

			elsif (opsel = "010") then
				-- MULT
				if (need_pn = '1') then
					psel1n <= '0';
					psel2n <= '1';
					state_nxt <= postn_st2;
				else
					psel1n <= '0';
					psel2n <= '0';
					state_nxt <= postn_st2;
				end if;
				-- End of MULT
				
			elsif (opsel = "011") then
				-- DIV
				fsel1n <= '1';
				fsel2n <= '0';
				psel1n <= '0';
				psel2n <= '1';
				state_nxt <= postn_st2;
				-- End of DIV
				
			elsif ((opsel = "110") or (opsel = "111")) then
				-- Norm AB
				fsel1n <= '0';
				fsel2n <= '0';
				psel1n <= '0';
				psel2n <= '1';
				state_nxt <= postn_st2;
				-- End of Norm AB
			end if; 

		when postn_st2 =>
			-- ADD/SUB
			fsel1n <= '0';
			fsel2n <= '0';
			psel1n <= '0';
			psel2n <= '0';
			state_nxt <= idle;
			-- End of ADD/SUB
	end case;

end process;
	
	sel <= fsel1 xor fsel2;
	fsm_sel <= sel;
	fsm_rdy <= psel1 xor psel2;

end Behavioral;

