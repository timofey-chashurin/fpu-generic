--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Multiplier is
	-- see the package declaration for signal descriptions
	generic (OpWidth: 	integer;
		 ExpWidth: 	integer;
		 FractWidth: 	integer);
	port(
		clk: 		in 	std_logic;
		rst: 		in 	std_logic;
		opsel:		in 	std_logic_vector(2 downto 0);
		op_a: 		in 	std_logic_vector(OpWidth-1 downto 0);
		op_b: 		in 	std_logic_vector(OpWidth-1 downto 0);
      		mfres_o: 	out 	std_logic_vector(FractWidth+1 downto 0);
		mexp_o: 	out 	std_logic_vector(ExpWidth downto 0);
		msign:		out	std_logic
);
end Multiplier;
 
 
architecture Behavioral of Multiplier is

signal 	fra: 	signed(FractWidth-1 downto 0) := (others => '0');
signal 	frb: 	signed(FractWidth-1 downto 0) := (others => '0');
signal 	expa: 	signed(ExpWidth-1 downto 0) := (others => '0');
signal 	expb: 	signed(ExpWidth-1 downto 0) := (others => '0');
signal	sign_a, sign_b: std_logic;

constant hval:	integer := (((2**ExpWidth-1)-1)/2);
constant lval:	integer := (-2**(ExpWidth-1));
constant width:	integer := ExpWidth;
constant above:	signed(ExpWidth-1 downto 0) := to_signed(hval, width);
constant below:	signed(ExpWidth-1 downto 0) := to_signed(lval, width);

begin
	fra <= signed(op_a(FractWidth-1 downto 0));
	frb <= signed(op_b(FractWidth-1 downto 0));
	expa <= signed(op_a(OpWidth-2 downto OpWidth-ExpWidth-1));
	expb <= signed(op_b(OpWidth-2 downto OpWidth-ExpWidth-1));
	sign_a <= op_a(OpWidth-1);
	sign_b <= op_b(OpWidth-1);
	
process (clk, rst, fra, frb, expa, expb, sign_a, sign_b)

variable expa_u: 	signed(ExpWidth downto 0);
variable expb_u: 	signed(ExpWidth downto 0);
variable fr_mres: 	unsigned(2*FractWidth-1 downto 0);
variable exp_mres: 	signed(ExpWidth downto 0);
variable rexp: 		signed(ExpWidth downto 0);
variable res: 		unsigned(2*FractWidth-1 downto 0); 
variable exp: 		signed(ExpWidth downto 0);

begin

if (clk = '1' and clk'event) then
	if (opsel = "010") then
		if (rst  = '1') then
			expa_u := (others => '0');
			expa_u := (others => '0');
			fr_mres := (others => '0');
			exp_mres := (others => '0');
			rexp := (others => '0');
			res := (others => '0');
			exp := (others => '0');
		else
			if(expa(ExpWidth-1) = '1') then
				expa_u := '1' & expa;
			else
				expa_u := '0' & expa;
			end if;
			if(expb(ExpWidth-1) = '1') then
				expb_u := '1' & expb;
			else
				expb_u := '0' & expb;
			end if;
			
			fr_mres := unsigned(fra) * unsigned(frb);
			rexp := expa_u + expb_u;

			res := fr_mres;
			exp := rexp;
		end if;
	end if;
end if;

	mfres_o <= std_logic_vector(res((FractWidth*2)-1 downto FractWidth-2));
	mexp_o <= std_logic_vector(exp);
	msign <= sign_a xor sign_b;

end process;

end Behavioral;
