--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable Floating-Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fixed_shifter is
	-- see the package declaration for signal descriptions
	generic(
		FractWidth:	integer;
		MuxWidth:	integer
		);
	port(
		s_in:	in	std_logic_vector(FractWidth+4 downto 0);
		shft:	in	std_logic;
		left:	in	std_logic;
		s_out:	out	std_logic_vector(FractWidth+4 downto 0)
		);
end fixed_shifter;
 
architecture parametr_arch of fixed_shifter is

signal 	zero: 	 std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal 	sh_rght: std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal 	sh_left: std_logic_vector(FractWidth+4 downto 0) := (others => '0');
signal 	sh_res:  std_logic_vector(FractWidth+4 downto 0) := (others => '0');

begin
	zero <= (others => '0');
	
	-- shift the operand right
	sh_rght <= zero(MuxWidth-1 downto 0) & s_in(FractWidth+4 downto MuxWidth);
	
	-- shift the operand left
	sh_left <= s_in(FractWidth+4-MuxWidth downto 0) & zero(MuxWidth-1 downto 0);
	
	-- shift left or right ?
	sh_res <= sh_left when left = '1' else sh_rght;
	
	-- Multiplexer
	s_out <= sh_res when shft = '1' else s_in;

end parametr_arch;
