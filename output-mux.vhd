--***********************************************************************
--* Copyright (C) 2018 Tymofii Chashurin <tymofii.chashurin@gmail.com>	*
--*									*
--* This file is part of "Generic Synthesizable-Floating Point Unit".	*
--*									*
--* "Generic Synthesizable Floating Point Unit"				*
--* can not be copied and/or distributed without			*
--* the express permission of Tymofii Chashurin				*
--***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.fpu_pack.all;

entity exponent_mux is
	-- see the package declaration for signal descriptions
	generic (OpWidth: 	integer;
 		 ExpWidth: 	integer;
		 FractWidth: 	integer
	 	);
	port(		
		opsel:		in	std_logic_vector(2 downto 0);
		ads_exp: 	in	std_logic_vector(ExpWidth downto 0);
		mul_exp: 	in	std_logic_vector(ExpWidth downto 0);
		div_exp: 	in	std_logic_vector(ExpWidth downto 0);
		fin_exp: 	out	std_logic_vector(ExpWidth downto 0);
		fract1_in:	in	std_logic_vector(FractWidth+4 downto 0);
		fract2_in:	in	std_logic_vector(FractWidth+4 downto 0); 
		sh_dn:		in	std_logic_vector(fr_log_width-1 downto 0);
		sh_pn:		in	std_logic_vector(fr_log_width-1 downto 0);
		fract_sel:	in	std_logic;
		left_sel:	in	std_logic;
		sh_out:		out	std_logic_vector(fr_log_width-1 downto 0);
		fr_out:		out	std_logic_vector(FractWidth+4 downto 0);
		left_so:	out	std_logic
 	    );
end;

architecture Behavioral of exponent_mux is

signal	exp:		std_logic_vector(ExpWidth downto 0) := (others => '0');
signal	fr_sel_i2: 	std_logic := '0';
signal	fr_sel_c: 	std_logic := '0';
signal	zero:		std_logic_vector(ExpWidth downto 0) := (others => '0');

begin

	zero <= (others => '0');

process (opsel, ads_exp, mul_exp, div_exp, zero)
begin
	case opsel is
		when "000" => fin_exp <= ads_exp;
		when "001" => fin_exp <= ads_exp;
		when "010" => fin_exp <= mul_exp;
		when "011" => fin_exp <= div_exp;
		when others	 => fin_exp <= zero;
	end case;
end process; 


process (fract1_in, fract2_in, fract_sel, sh_pn, fract_sel,
			left_sel, fr_sel_i2, fr_sel_c, sh_dn, opsel)
begin
	fr_sel_c <= fract_sel or fr_sel_i2;

	if (fr_sel_c = '1') then
		fr_out <= fract2_in;
	else
		fr_out <= fract1_in;
	end if;
	
	if (fr_sel_c = '1') then
		sh_out <= sh_pn;
	else
		sh_out <= sh_dn;
	end if;

	if ((opsel = "000") or (opsel = "001")) then
		fr_sel_i2 <= '0';
	else
		fr_sel_i2 <= '1';
	end if;
	
	left_so <= fr_sel_c and left_sel;
end process;

end Behavioral;


